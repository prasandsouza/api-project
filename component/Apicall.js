import React, { Component } from 'react'
import axios from 'axios'
import Loader from './Loader'
import './apicall.css'
// import Products from './Products'
class Apicall extends Component {
    constructor(props) {
        super(props)
        this.appState = {
            Loading: 'loading',
            loaded: 'loaded',
            error: 'error',
        }
        this.state = {
            status: this.appState.Loading,
            APIData: '',
            APIError: '',
            data: []
        }
    }

    componentDidMount = () => {
        axios.get('https://fakestoreapi.com/products')
            .then((event) => {
                console.log(event.data)
                this.setState({
                    status: this.appState.loaded,
                    data: event.data
                })
            })
            .catch((err) => {
                console.log(err);
                this.setState({
                    status: this.appState.error
                })
            })

    }
    render() {
        return (
            <div className='mainContainer'>
                <h1> Our products</h1>
                {this.state.status === this.appState.Loading ? <Loader message="LOADING..." /> : ''}

                {this.state.status === this.appState.error ? <div> <h1>  Error occured while fetching </h1> </div> : ''}
                <div className='product-container'>
                    {this.state.status === this.appState.loaded ? <div>{this.state.data.length === 0 ? <h1> NO PRODUCT AVAILABLE </h1> :
                        this.state.data.map((products) => {
                            return (
                                <div key={products.id} className='IndividualProduct'>
                                    <p className='ProductCategory'> {products.category} </p>
                                    <img src={products.image} className='ProductImage' />
                                    <h2 className='Producttitle'> {products.title}</h2>
                                    <p className='ProductDescription'> {products.description}</p>
                                    <div>
                                        <p className="productsrate"> {products.rating.rate} &#9733;  </p>
                                        <p className="productCount">  (out of {products.rating.count} review)</p>
                                    </div>
                                    <p className='ProductPrice'> &#36; {products.price} </p>
                                </div>
                            )
                        })
                    }</div> : ''}
                </div>

            </div>
        )
    }
}

export default Apicall