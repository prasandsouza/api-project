import logo from './logo.svg';
import './App.css';
import Apicall from './component/Apicall';

function App() {
  return (
    <div className="App">
      <Apicall/>
    </div>
  );
}

export default App;
